"""
SW 2022-01-25

Quick script to post-proc the bond puller logs

Can be imported or run through CLI
"""

import json
import argparse
import sys
import re

import bond_puller

def try_convert(input_str):
    """
    Quick convenience func for checking if string can be cast to int first, then float
    """
    try:
        result = int(input_str)
        return result
    except ValueError:
        pass
    try:
        result = float(input_str)
        return result
    except ValueError:
        pass

    return input_str


def run_post_proc(input_log_path, output_path, test_utc_datetime_str, comments, metadata_dict,):
    """
    Runs the post proc: takes lines from bond_puller, post procs and converts to json file

    Also returns the dict created
    """
    result = {}
    result["origin"] = input_log_path
    result["datetime_of_test_utc"] = test_utc_datetime_str
    result["metadata_dict"] = metadata_dict
    result["comments"] = comments
    result["__help__"] = (
        "Origin is path of logfile used in post proc,"
        "datetime_of_test_utc is the datetime in UTC of the bond pulling test,"
        "metadata dict is a catchall dict for metadata such as bond pull force etc",
        "comments is a field used for adding comments as required",
        "pull_test_results is a Nx3 list featuring pull number within this batch, pull strength,",
        "break code"
    )
    result["pull_test_results"] = []
    with open(input_log_path, 'r') as a_file:
        line_regex = re.compile(r"(\d{4})\s+GD \d\s+(\d+\.\d+)g break_code=(\d+)")
        for line in a_file:
            match_result = line_regex.match(line)
            if not match_result:
                # Sometimes we can get a wrong value due to bad measurement etc
                # A-Z section is PASS for fine, OR for something bad?
                # Pass usually happens because someone forgot to turn off NDT
                if re.match(r"(\d{4})\s+[A-Z].*(\d+\.\d+)g break_code=\d", line):
                    extra_match_result = re.match(r"(\d{4})\s+[A-Z].*(\d+\.\d+)g break_code=\d", line)
                    # Force in break code 6 for NDT stuff
                    line_results = extra_match_result.groups() + (6,)
                    result["pull_test_results"].append(line_results)
                    continue
                else:
                    print(f"Issue parsing line {line} of logfile {input_log_path}")
                    raise ValueError

            # Cast to list to be mutable, and to later create a list of lists
            line_results = list(match_result.groups())
            line_results[0] = int(line_results[0])
            line_results[1] = float(line_results[1])
            line_results[2] = int(line_results[2])

            result["pull_test_results"].append(line_results)

    with open(output_path, 'w') as a_file:
        json.dump(result, a_file)

    return result

def check_metadata(metadata_info):
    """
    Given metadata info check that it's appropriate
    Should be a list of stuff that matches the regex "([A-z0-9_\\.]+)=([A-z0-9_\\.]+)"
    """
    metadata_dict = {}
    metadata_regex = re.compile(r"([A-z0-9_\.]+)=([A-z0-9_\.]+)")
    for meta_arg in metadata_info:
        match_result = metadata_regex.match(meta_arg)
        if not match_result:
            print("Metadata not of the form X=Y with regex " + r"([A-z0-9_\.]+)=([A-z0-9_\.]+)")
            raise ValueError
        key, value = match_result.groups()
        value = try_convert(value)
        metadata_dict[key] = value
    return metadata_dict

def main():
    """
    Main CLI entrypoint
    """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "input_log_path", 
        help="Path to bond puller log file from bond_puller.py"
    )
    parser.add_argument("output_path", help="Output JSON path")
    parser.add_argument(
        "time_of_test",
        help="Time of test, UTC timezome, format YYYY-MM-DDTHH:MM:SS"
    )
    parser.add_argument("--comments", help="Test comments", default="")
    parser.add_argument("--metadata",
        help="Additional metadata of the form X=Y, no spaces. For power/force/deformation etc",
        nargs="+",
        default=[],
    )
    parser.add_argument("--input_metadata_dict",
        help="Whether to fill metadata dict via user inputs", action="store_true"
    )

    args = parser.parse_args()

    # Check metadata stuff now
    if not args.input_metadata_dict:
        metadata_dict = check_metadata(args.metadata)
    else:
        metadata_dict = bond_puller.input_metadata_dict()

    run_post_proc(
        args.input_log_path, args.output_path, args.time_of_test, args.comments, metadata_dict,
    )

    sys.exit(0)

if __name__ == "__main__":
    main()
