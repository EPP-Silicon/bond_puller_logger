"""
SW 2022-01-23

Bond puller logger

"""



import sys
import argparse
import datetime
import os

import serial
import serial.tools.list_ports as list_ports

import post_proc_log

COMPORT_NAME = "COM3"

def get_port():
    """
    Quick func to find the cmport associated to the USB/Serial converter being used
    """
    ports = list_ports.comports()
    device_descs = [
        "ATEN USB to Serial Bridge",
        "USB-Serial Controller D", # What the logger is called on linux
    ]
    result = None
    for port in ports:
        for device_desc in device_descs:
            if device_desc in port.description:
                result = port.device
                break
        if result is not None:
            break
    return result

def run_logging(puller_conn, file_obj):
    """
    Main logging part, uses serial connection and output file object to write original puller
    output code plus the bond failure code.
    Some minor rewriting of puller output.
    """
    count_logger = 1
    while True:
        this_input = input("Q/q to quit, otherwise take new line from puller: ")
        if this_input.lower() == "q":
            # Quit response
            break
        # Read from machine
        print(f"Pull now (this pull is number {count_logger}, not the same as machine idx)")
        count_logger = count_logger + 1
        this_machine_output = puller_conn.read_until(b"\r\n\r")
        # While true to parse code
        while True:
            input_display_text = (
                "Select break type; 1 break src, 2 break dest, 3 midspan, 4 peel src, 5 peel dest,"
                "6 general catch all for bad bond\n"
            )
            this_input = input(input_display_text)
            try:
                this_code = int(this_input)
                if this_code not in [1,2,3,4,5,6]:
                    print("Not a valid break code (not in 1-5), try again")
                else:
                    break
            except ValueError:
                print("Not a valid break code (can't parse to int) try again")
        # Write pull test to file
        this_data = this_machine_output[:-3].decode() + f" break_code={this_code}\n"
        file_obj.write(this_data)

    return

def input_metadata_dict():
    """
    Asks some questions, returns metadata dict
    """
    metadata_dict = {}
    comments = input("Please input any commments for test\n")
    metadata_dict["comments"] = comments
    power = input("Please input power in %, digits only: ")
    metadata_dict["power"] = power
    force = input("Please input force in cN, digits only: ")
    metadata_dict["force"] = force
    deformation = input("Please input max deformation in %, digits only: ")
    metadata_dict["deformation"] = deformation
    max_time = input("Please input max bonding time in ms, digits only: ")
    metadata_dict["max_time"] = max_time
    loop_length = input("Please input loop length, from numbers need bondsites on wirebonder (mm): ")
    metadata_dict["loop_length"] = loop_length
    loop_height = input("Please input loop height, from machine settings on wirebonder (mm): ")
    metadata_dict["loop_height"] = loop_height
    source_angle = input("Please input the angle of the loop at the source side (measure on CMM)(deg): ")
    metadata_dict["source_angle"] = source_angle
    dest_angle = input(
        "Please input the angle of the loop at the destination side (measure on CMM)(deg): "
    )
    metadata_dict["dest_angle"] = dest_angle
    surface_code = input("Please input surface code or name: ")
    metadata_dict["surface_code"] = surface_code
    machine_factors = input("Please input machine factors value %, digits only: ")
    metadata_dict["machine_factors"] = machine_factors
    operator_name = input("Please input operator name: ")
    metadata_dict["operator_name"] = operator_name

    return metadata_dict

def run_post_proc(puller_log_path):
    """
    Runs the post proc stage with required input
    """
    output_path = os.path.join(
        os.path.split(puller_log_path)[0],
        os.path.splitext(os.path.split(puller_log_path)[1])[0] + ".json"
    )
    if output_path == puller_log_path:
        print("Issue, puller log path is same as output path")
        raise ValueError
    now_utc = datetime.datetime.utcnow()
    test_utc_datetime_str = now_utc.strftime("%Y-%m-%dT%H:%M:%S")


    metadata_dict = input_metadata_dict()

    post_proc_log.run_post_proc(
        puller_log_path, output_path, test_utc_datetime_str,
        metadata_dict["comments"], metadata_dict
    )
    return

def main():
    """
    Main CLI entry point
    """
    print("Start")

    parser = argparse.ArgumentParser()
    parser.add_argument("output_file", help="Path to file to write results, ie. file.txt")
    parser.add_argument("--overwrite_file", help="Whether to overwrite file", action="store_true")
    parser.add_argument("--no_post_proc", help="Skip post proc stage", action="store_true")

    args = parser.parse_args()
    if not args.overwrite_file and os.path.exists(args.output_file):
        print("File exists and not set to overwrite, quitting")
        sys.exit(1)

    # Make sure we don't try to write .json straight away
    if args.output_file[-5:] == ".json":
        print("File shouldn't end with .json")
        sys.exit(2)
    if os.path.exists(args.output_file):
        print("File already exists")
        sys.exit(3)

    com_port = get_port()
    print("COM port used is {}".format(com_port))

    if com_port is None:
        print("No com port found")
        raise ValueError

    puller_conn = serial.Serial(com_port)
    with open(args.output_file, 'w') as a_file:
        run_logging(puller_conn, a_file)

    puller_conn.close()

    # Optional post proc
    if not args.no_post_proc:
        run_post_proc(args.output_file,)

    print("End")
    sys.exit(0)


if __name__ == "__main__":
    main()
